## 01 Primeiros Passos
Você precisa ter o [nodejs](https://nodejs.org/) instalado na máquina para rodar os comandos node e npm.

Ao clonar o repositório basta abrir o terminal dentro da pasta do projeto e executar o comando `npm install`, para instalar todos os pacotes e dependências para rodar o projeto e depois executar o comando `gulp` para executar todas as tarefas.

O **gulpfile.js** é o arquivo que comanda as tarefas do gulp.
A pasta **node_modules** nunca é enviada para o git ou para o servidor, por isso a pasta tem que estar no **.gitignore**

## 02 Editando os Arquivos
Todos os arquivos observados pelo git estão na pasta **assets**, ou seja, você sempre vai mexer nesses arquivos. Os arquivos da pasta **dist** não vão para o git, pois são os arquivos finais já compactados para produção, você não pode editá-los diretamente.

Todos os arquivos da pasta **assets** estão sendo observados pela tarefa `watch` do gulp, ou seja, quando você fizer uma alteração em qualquer arquivo e salvar(CTRL + S), seja estilo ou script, o gulp vai executar as tarefas e exportá-los já para produção na pasta **dist**

####Exemplo:
O arquivo style.scss está na pasta **assets/style**, quando for salvo ele irá criar um arquivo comprimido style.css na pasta **dist/style** que será o arquivo final chamado pelo link <link rel="stylesheet" src="dist/style/style.css">

## 03 Tarefas Automatizadas
Para executar todas as tarefas basta executar o comando `gulp`. Se preferir pode executar uma tarefa individualmente executando os códigos abaixo.

####Tarefa para emular servidor local no browser
`gulp browsersync`

####Tarefa para processar o SASS para CSS
`gulp style`

####Tarefa para concatenar os arquivos JS em um só
`gulp concat`

####Tarefa para comprimir as IMAGENS
`gulp images`







