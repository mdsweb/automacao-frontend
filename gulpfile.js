//Requer o pacote gulp
var gulp = require('gulp');
//Requer o pacote gulp-sass
var sass = require('gulp-sass');
//Requer o pacote gulp-autoprefixer
var autoprefixer = require('gulp-autoprefixer');
//Requer o pacote browser-sync
var browsersync = require('browser-sync').create();
//Requer o pacote gulp-concat
var concat = require('gulp-concat');


//Style task - para arquivos css
gulp.task('style', function() {
   //Pega os arquivos
   return gulp.src(['assets/style/*.css', './assets/style/*.scss'])
   //Aplica o sass neles, compresso e exibe erro caso haja algum
   .pipe(sass({
      outputStyle: 'compressed'}).on('error', sass.logError))   
   //Depois que processou pra css ele executa o autoprefixer
   .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
  }))
  //Finalmente envia os arquivos limpos para dist
  .pipe(gulp.dest('./dist/style'))
  .pipe(browsersync.stream());
});

//Concat Scripts - agrupa todos scripts dentro do main.js
gulp.task('concat', function() {
   return gulp
   .src([
      'assets/script/*.js',      
      //! important ! Puxa o jquery.js do node_modules
      'node_modules/jquery/dist/jquery.min.js',
      //! important ! Puxa o bootstrap.js do node_modules
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
      'assets/script/plugins/*.js'
   ])   
   .pipe(concat('main.js'))
   .pipe(gulp.dest('dist/script'));
});

//Browser Sync
gulp.task('browsersync', function() {
   browsersync.init({
       server: {
           baseDir: "./"
       }
   });
});

//Watch Task - observa as alterações
gulp.task('watch', function() {
   gulp.watch('assets/style/*.scss', gulp.series('style',));
   gulp.watch('assets/script/**/*.js', gulp.series('concat'));
   //Da o reload na página quando altera arquivo html
   gulp.watch(['*.html','*.php']).on('change', browsersync.reload)
});

//Default Tasks
gulp.task('default', gulp.parallel('browsersync','style','concat','watch'));


